// Scripts for firebase and firebase messaging
importScripts("https://www.gstatic.com/firebasejs/8.2.0/firebase-app.js");
importScripts("https://www.gstatic.com/firebasejs/8.2.0/firebase-messaging.js");


const firebaseConfig = {
  apiKey: "AIzaSyCasq7FRg8_1rJ0kOsxpmaHHRiOFGXs7xo",
  authDomain: "test-838f7.firebaseapp.com",
  projectId: "test-838f7",
  storageBucket: "test-838f7.appspot.com",
  messagingSenderId: "529512098296",
  appId: "1:529512098296:web:990940f67ef16aa12a4e0d",
  measurementId: "G-P9C735B00N"
};


firebase.initializeApp(firebaseConfig);

const messaging = firebase.messaging();

messaging.onBackgroundMessage(function (payload) {
  console.log("Received background message ", payload);

  const notificationTitle = payload.notification.title;
  const notificationOptions = {
    body: payload.notification.body,
    icon: "/logo192.png",
  };

  // eslint-disable-next-line no-restricted-globals
  return self.registration.showNotification(
    notificationTitle,
    notificationOptions
  );
});