import firebase from "firebase/app";
import "firebase/messaging";

// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyCasq7FRg8_1rJ0kOsxpmaHHRiOFGXs7xo",
    authDomain: "test-838f7.firebaseapp.com",
    projectId: "test-838f7",
    storageBucket: "test-838f7.appspot.com",
    messagingSenderId: "529512098296",
    appId: "1:529512098296:web:990940f67ef16aa12a4e0d",
    measurementId: "G-P9C735B00N"
};

firebase.initializeApp(firebaseConfig);

const messaging = firebase.messaging();
const { REACT_APP_VAPID_KEY } = process.env;
const publicKey = REACT_APP_VAPID_KEY;

// Get token form Console
export const getToken = async () => {
  let currentToken = "";
  try {
    currentToken = await messaging.getToken({ vapidKey: publicKey });
    if (currentToken) {
      console.log("Succes token:",currentToken)
    } else {
      console.log("Failed token:",currentToken)
    }
  } catch (error) {
    console.log("An error occurred while retrieving token. ", error);
  }
  return currentToken;
};

//Message received from Firebase
export const onMessageListener = () =>
  new Promise((resolve) => {
    messaging.onMessage((payload) => {
      resolve(payload);
    });
  });
