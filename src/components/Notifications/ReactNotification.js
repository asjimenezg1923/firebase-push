import React from 'react';
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import styled from "styled-components"

//Notification design content
const Container=styled.div`
  background-color:rgba(89, 144, 223, 0.8);
  color: white;
  box-shadow: 0px 10px 13px -7px #000000, 0px 0px 12px 3px rgba(29,176,255,0.55);
  border-radius: 30px ;
  display: grid;
`

export const ReactNotificationComponent = (props) => {
  //Message properties by props
  const Msg = () => (
      <Container>
        <h1>{props.title}</h1>
        <p>{props.body}</p>
      </Container>
    )
  let test =props.title  
  //Rectify the message is not an empty string
    if(test.length>0)
    toast(Msg)
    return (
      <div>
        <ToastContainer 
          autoClose={3000}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss={false}
          draggable
          pauseOnHover
        />
      </div>
    );
};




