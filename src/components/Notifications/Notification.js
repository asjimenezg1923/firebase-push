import { useState } from "react";
import { onMessageListener} from "../../firebaseInit";
import { ReactNotificationComponent } from "./ReactNotification";

export const Notification=()=>{
  //useState to rectify that the content was received
  const [show, setShow] = useState(false);
  //useState to store the content of the message (notification)
  const [notification, setNotification] = useState({ title: "", body: "" });
  console.log(show, notification);
  onMessageListener()
    .then((payload) => {
      setShow(true);
      setNotification({
        title: payload.notification.title,
        body: payload.notification.body,
      });
      console.log("",payload);
    })
    .catch((err) => console.log("Failed: ", err));  
    
  return (
    <div className="App">
      {show ? (
        <ReactNotificationComponent
          title={notification.title}
          body={notification.body}
          />
      ) : (
        <></>
      )}
    </div>
  );
}




