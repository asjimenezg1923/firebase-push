import "./App.css";
import React from "react";
import  {Notification}  from "../src/components/Notifications/Notification";
import { getToken } from "./firebaseInit";
getToken()

export const App = ()=> {
  return (
    <>
    <Notification />
    </>
  );
}
